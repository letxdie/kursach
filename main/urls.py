from django.urls import path
from . import views

urlpatterns = [

    path('', views.index, name='index'),
    path('auth', views.auth, name="auth"),
    path('add', views.add, name="add"),
    path('info', views.info, name="info"),
    path('add_port', views.add_port, name="add_port"),
    path('add_sudno', views.add_sudno, name="add_sudno"),
    path('list_port', views.list, name="list"),
    path('add_p', views.add_p, name="add_p"),
    path('add_user', views.add_user, name="add_user"),
    path('user_list', views.user_list, name="user_list"),
    path('del_user', views.user_list, name="del_user"),
    path('port_list', views.port_list, name="port_list"),
    path('transp_list', views.transp_list, name="transp_list"),
    path('sudno_list', views.sudno_list, name="sudno_list"),
    path('transp_list_local', views.transp_list_local, name="transp_list_local")

]
