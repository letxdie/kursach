from django.shortcuts import render
import json


def auth(request):
    users = open('user.json')
    user = json.load(users)
    with open('sudno.json', 'r', encoding='utf-8') as fh:
        sudno = json.load(fh)
    with open('port.json', 'r', encoding='utf-8') as fh:
        port = json.load(fh)
    x = open('transp.json')
    dat = json.load(x)
    x.close()
    data = {
        "username": "admin",
        "password": "12345"
    }
    if request.POST:
        if data['username'] == request.POST.get('username') and data["password"] == request.POST.get('password'):
            return render(request, 'admin.html', {"port": port, "sudno": sudno})
        else:
            for i in user:
                if i['p_login'] == request.POST.get('username') and i['p_password'] == request.POST.get('password'):
                    global port_str
                    port_str = i['p_port']
                    return render(request, 'half_admin.html',
                                  {"p_port": i['p_port'], "p_name": i['p_name'], "port": port,
                                   "sudno": sudno, "a": "Добро пожаловать,", "b": ", ваш порт"})
    return render(request, 'homepage.html', {'error': 'ACCESS DENIED!!', "x": dat})


def index(request):
    x = open('transp.json')
    dat = json.load(x)
    x.close()
    return render(request, 'homepage.html', {'x': dat})


def add_port(request):
    if request.POST:
        with open('sudno.json', 'r', encoding='utf-8') as fh:
            sudno = json.load(fh)
        port_dict = {"port": request.POST.get('port')}
        with open('port.json', 'r', encoding='utf-8') as fh:
            port = json.load(fh)
            port.append(port_dict)
            fh.close()
        with open('port.json', 'w', encoding='utf-8') as fh:
            json.dump(port, fh)
            fh.close()
        return render(request, 'admin.html', {'port': port, "sudno": sudno, "y": "Данные успешно добавлены"})


def add_sudno(request):
    if request.POST:
        with open('port.json', 'r', encoding='utf-8') as fh:
            port = json.load(fh)
        sudno_dict = {
            "s_name": request.POST.get('s_name'),
            "s_model": request.POST.get('s_model'),
            "s_type": request.POST.get('s_type'),
            "s_char": request.POST.get('s_char')}
        fh.close()
        with open('sudno.json', 'r', encoding='utf-8') as fh:
            sudno = json.load(fh)
            sudno.append(sudno_dict)
        with open('sudno.json', 'w', encoding='utf-8') as fh:
            json.dump(sudno, fh)
            fh.close()
        return render(request, 'admin.html', {"port": port, "sudno": sudno, "z": "Данные успешно добавлены"})


def add(request):
    if request.POST:
        s_model = ''
        s_char = ''
        s_type = ''
        with open('sudno.json', 'r', encoding='utf-8') as fh:
            sudno = json.load(fh)
            for i in sudno:
                if request.POST.get('s_name') == i['s_name']:
                    s_model = i['s_model']
                    s_char = i['s_char']
                    s_type = i['s_type']
        with open('port.json', 'r', encoding='utf-8') as fh:
            port = json.load(fh)
        with open('transp.json', 'r', encoding='utf-8') as fh:
            dat = json.load(fh)
            add_json = {
                "s_name": request.POST.get('s_name'),
                "s_model": s_model,
                "s_type": s_type,
                "s_char": s_char,
                "p_otprav": request.POST.get('p_otprav'),
                "p_prib": request.POST.get('p_prib'),
                "t_otprav": time(request.POST.get('t_otprav')),
                "t_prib": time(request.POST.get('t_prib')),
                "key": len(dat) + 26112000
            }
            dat.insert(0, add_json)
            fh.close()
        with open('transp.json', 'w', encoding='utf-8') as fh:
            json.dump(dat, fh)
            fh.close()
        return render(request, 'admin.html', {'x': "Данные успешно добавлены", "port": port, "sudno": sudno})


def time(d_time):
    string = d_time
    string = string.replace('T', ' ')
    return string


def add_p(request):
    with open('sudno.json', 'r', encoding='utf-8') as fh:
        sudno = json.load(fh)
    with open('port.json', 'r', encoding='utf-8') as fh:
        port = json.load(fh)
    if request.POST:
        p_name = request.POST.get('p_name')
        p_login = request.POST.get('p_login')
        p_password = request.POST.get('p_password')
        p_port = request.POST.get('p_port')
        user_dict = {
            "p_name": p_name,
            "p_login": p_login,
            "p_password": p_password,
            "p_port": p_port
        }
        with open('user.json', 'r', encoding='utf-8') as fh:
            user = json.load(fh)
            user.append(user_dict)
            fh.close()
        with open('user.json', 'w', encoding='utf-8') as fh:
            json.dump(user, fh)
            fh.close()
        return render(request, 'admin.html', {'g': "Данные успешно добавлены", "port": port, "sudno": sudno})


def list(request):
    with open('port.json', 'r', encoding='utf-8') as fh:
        port = json.load(fh)
    return render(request, "list.html", {"port": port})


def info(request):
    local_transp = []
    if request.POST:
        x = request.POST.get('port')
        with open('transp.json', 'r', encoding='utf-8') as fh:
            data = json.load(fh)
            for i in data:
                if i['p_otprav'] == x or i['p_prib'] == x:
                    local_transp.append(i)
            if not local_transp:
                return render(request, "info.html", {"x": "Нет данных"})
            else:
                return render(request, 'info.html', {'x': x, "y": local_transp})


def add_user(request):
    if request.POST:
        s_model = ''
        s_char = ''
        s_type = ''
        with open('sudno.json', 'r', encoding='utf-8') as fh:
            sudno = json.load(fh)
            for i in sudno:
                if request.POST.get('s_name') == i['s_name']:
                    s_model = i['s_model']
                    s_char = i['s_char']
                    s_type = i['s_type']
        with open('port.json', 'r', encoding='utf-8') as fh:
            port = json.load(fh)
        with open('transp.json', 'r', encoding='utf-8') as fh:
            data = json.load(fh)
            add_json = {
                "s_name": request.POST.get('s_name'),
                "s_model": s_model,
                "s_type": s_type,
                "s_char": s_char,
                "p_otprav": request.POST.get('p_otprav'),
                "p_prib": request.POST.get('p_prib'),
                "t_otprav": time(request.POST.get('t_otprav')),
                "t_prib": time(request.POST.get('t_prib')),
                "key": len(data) + 26112000
            }
            data.insert(0, add_json)
            fh.close()
        with open('transp.json', 'w', encoding='utf-8') as fh:
            json.dump(data, fh)
            fh.close()
        return render(request, 'half_admin.html',
                      {'x': "Данные успешно добавлены", "port": port, "sudno": sudno, 'p_port': port_str})


def user_list(request):
    x = open('user.json')
    dat = json.load(x)
    x.close()
    if request.POST:
        del_user = request.POST.get('del_user')
        for i in dat:
            if i['p_name'] == del_user:
                del dat[dat.index(i)]
                with open('user.json', 'w', encoding='utf-8') as fh:
                    json.dump(dat, fh)
                    fh.close()
                x = open('user.json')
                dat = json.load(x)
                x.close()
    return render(request, 'user_list.html', {'user': dat})


def port_list(request):
    x = open('port.json')
    dat = json.load(x)
    x.close()
    if request.POST:
        del_port = request.POST.get('del_port')
        for i in dat:
            if i['port'] == del_port:
                del dat[dat.index(i)]
                with open('port.json', 'w', encoding='utf-8') as fh:
                    json.dump(dat, fh)
                    fh.close()
                x = open('port.json')
                dat = json.load(x)
                x.close()
    return render(request, 'port_list.html', {'port': dat})


def transp_list(request):
    x = open('transp.json')
    dat = json.load(x)
    x.close()
    if request.POST:
        del_transp = request.POST.get('del_transp')
        for i in dat:
            if i['key'] == int(del_transp):
                del dat[dat.index(i)]
                with open('transp.json', 'w', encoding='utf-8') as fh:
                    json.dump(dat, fh)
                    fh.close()
                x = open('transp.json')
                dat = json.load(x)
                x.close()
    return render(request, 'transp_list.html', {'transp': dat})


def sudno_list(request):
    x = open('sudno.json')
    dat = json.load(x)
    x.close()
    if request.POST:
        del_sudno = request.POST.get('del_sudno')
        for i in dat:
            if i['s_name'] == del_sudno:
                del dat[dat.index(i)]
                with open('sudno.json', 'w', encoding='utf-8') as fh:
                    json.dump(dat, fh)
                    fh.close()
                x = open('sudno.json')
                dat = json.load(x)
                x.close()
    return render(request, 'sudno_list.html', {'sudno': dat})


def transp_list_local(request):
    x = open('transp.json')
    dat = json.load(x)
    x.close()
    if request.POST:
        del_transp = request.POST.get('del_transp')
        for i in dat:
            if i['key'] == int(del_transp):
                del dat[dat.index(i)]
                with open('transp.json', 'w', encoding='utf-8') as fh:
                    json.dump(dat, fh)
                    fh.close()
                x = open('transp.json')
                dat = json.load(x)
                x.close()
    return render(request, 'transp_list_local.html', {'transp': dat, 'ports': port_str})
